This application is designed to test the two factor identification API from Nexmo. 
You can run the application by using python nexmoTest.py
then follow the instructions in the terminal. 

This is a terminal-based application. 
Make sure to not answer "N" to "did you receive the code" and then "Y" to "Do you want to receive a new code" within the 30 seconds following the first request